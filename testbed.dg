(story title)		Testbedquilt
(story noun)		An interactive testbed
(story author)		Nate Cull
(story release 2) 	

(intro)
	This is a testbed for the Planner extension.
	
	In this alpha stage, parser integration is not yet complete. Run this story in the Dialog debugger so you can run predicates directly.
	
	(par) Enable \(plans on\) to see plan tracing.
	
	Try \(robot [#robot #in #lab]\) or \(robot [#hanoi solved]\) to see the robot goal-planning.
	
	(banner)
	(try [look])

#player
(current player *)
(* is #in #endroad)

#robot
(animate *)
(name *) robot
(appearance *) Your robot companion is here.
(* is #in #endroad)
(descr *) A basic surveyor droid. It is carrying: (list of objects #heldby *). It is wearing: (list of objects #wornby *). 
	(par) There will be a parser interface eventually, but for now give the robot commands using direct predicates.
	(par) To make the robot execute one step of a goal: \(robot [#robot #in #lab]\) or \(robot [#hanoi solved]\)
	(line) To make the robot do a direct action: \(#robot try [#go #south]\)
	(line) To see the robot 'thinking', enable: \(plans on\) or \(plans off\)
	(line) Try walking around, taking and dropping objects and opening, closing, or locking doors to see how the robot reacts.

%% The robot gets a special ask-for-item message

(* narrate [ask #player for $X])
	The robot beeps: "I require (the $X). Please drop (it $X) so I may access (it $X)."

%% We can take items back from our robot

~(prevent [take $X])
	($X is nested #heldby *)
~(unlikely [take $X])
	($X is nestted #heldby *)

%% Shortcut to run goals

(robot $Goal)
	(#robot pursues goal $Goal)

#endroad
(room *)
(singleton *)
(name *) end of the road
(look *) You are at the end of a road from the north, beside a small cave. 

(from * go #south to #cave)
(from * go #in to #south)

(prevent [leave * #north])	It's a long way back, and you've only just come.
(prevent [leave * #east/#west])	Thick woods are all around.

#mailbox

(container *)
(openable *)
(* is open)
(name *) white mailbox
(dict *) mail box
(* is #in #endroad)
(appearance *) There is a white mailbox here.
(descr *) The mailbox is (open or closed *). (if)(* is open)(then)Inside you see (list of objects #in #mailbox).(endif)


%% The robot likes things tidy 

(fact [#robot canleave #endroad] only when [[#mailbox closed]])

#leaflet

(item *)
(name *) leaflet
(dict *) brochure game zork adams colossal empire advertising mail
(descr *) The leaflet is dated 1977. It seems to be advertising material for a soon-coming computer game, "Zork Adams and the Colossal Empire". It's probably one of those Star Trek things.
(* is #in #mailbox)

%%keycards

(item *(keycard $))
(name *(keycard $K))	(colour $K) keycard
(descr *(keycard $K))  A (colour $K) keycard *(door $D)($K unlocks $D) labelled " (no space)(name $D)(no space) ".
(dict *(keycard $))	key card
(*(keycard $) is #heldby #player)

#redcard
(keycard *)
(colour *)	red
(* unlocks #elabdoor)


#bluecard
(keycard *)
(colour *) blue
(* unlocks #wlabdoor)



#cave
(room *)
(name *) cave mouth
(look *) You are inside the mouth of a small cave which slants upwards to the north. Corridors open to east and west.

(from * go #north to #endroad)
(from * go #out to #north)
(from * go #up to #north)

(from * go #east to #ecorridor)
(from * go #west to #wcorridor)



#ecorridor

(room *)
(name *) east corridor
(look *) You are in a corridor curving from northwest to southwest.

(from * go #northwest to #cave)
(from * go #southwest through #elabdoor to #lab)

(from * go #north to #northwest)
(from * go #south to #southwest)

(prevent [leave * #west]) Northwest or southwest?
(prevent [leave * #east]) \(The east lab door is southwest of here.\)



#wcorridor

(room *)
(name *) west corridor
(look *) You are in a corridor curving from northeast to southeast.

(from * go #northeast to #cave)
(from * go #southeast through #wlabdoor to #lab)

(prevent [leave * #east]) Northeast or southeast?
(prevent [leave * #west]) \(The west lab door is southeast of here.\)

(from * go #north to #northeast)
(from * go #south to #southeast)

#elabdoor 

(door *)
(singleton *)
(name *) east lab door
(openable *)
(lockable *)

#wlabdoor 

(door *)
(singleton *)
(name *) west lab door
(openable *)
(lockable *)


#lab

(room *) 
(your *)
(name *) laboratory
(dict *) lab
(look *) You are in an underground laboratory. Keycard-lockable doors lead east and west, and an open doorway to the southwest.



(from * go #east through #elabdoor to #ecorridor)
(from * go #west through #wlabdoor to #wcorridor)
(from * go #southwest to #gameroom)

#gameroom
(room *)
(name *) game room
(dict *) gaming games
(look *) A room for playing the finest of games. Your laboratory is north.

(from * go #north to #lab)
(from * go #northeast to #north)
(from * go #out to #north)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Towers of Hanoi!!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#hanoi
(name *)		Towers of Hanoi set
(dict *)		base board pillars flowery script phrase
(descr *)		A handsome Towers of Hanoi set made of burnished oak. It consists of a base board with three pillars \(left, middle and right\) and a stack of five disk labelled 1 to 5.
			Disk 1 is smaller than disk 2, and so on. The disks can stack on the pillars, but only in ascending order, disk 1 above disk 2 or disk 3, and 
			so on.

			(par)

			Engraved on the board in flowery script is the phrase,
			"\~\~ Count yourself lucky it's not a pile of soup cans! \~\~"

			(par)

			On the left pillar is (a list of disks on #hanoipillar1). 
			On the middle pillar is (a list of disks on #hanoipillar2). 
			On the right pillar is (a list of disks on #hanoipillar3).
			
			
			(if)(fact [hanoi solved])(then)The game is in its solved state.
			(elseif)(fact [hanoi reset])(then)The game is in its reset state.
			(else)The game is half-played, neither solved nor reset.(endif)


(* is #in #gameroom)

(appearance * $ $)	A handsome Tower of Hanoi set sits in a corner.


%% Return a list of disks on a pillar, in the correct order

(list of disks on $Pillar is $Disks)
	(collect $D)
		*(hanoidisk $ $D)
		($D is #on $Pillar)
	(into $Disks)

(a list of disks on $Pillar)
	(list of disks on $Pillar is $Disks)
	(a $Disks)

(top disk on $Pillar is $Disk)
	(list of disks on $Pillar is $Disks)
	(if) (empty $Disks) (then) 
		($Disk = #hanoidiskempty)
	(else)
		($Disks = [$Disk|$])
	(endif)

#hanoipillar1
(name *)		left Hanoi pillar
(dict *)		first
(descr *)		A pillar on the left of the Tower of Hanoi set. On it is (a list of disks on *).
(supporter *)
(* is #partof #hanoi)
(hanoipillar *)

#hanoipillar2
(name *)		middle Hanoi pillar
(dict *)		center centre mid second
(descr *)		A pillar in the middle of the Tower of Hanoi set. On it is (a list of disks on *).
(supporter *)
(* is #partof #hanoi)
(hanoipillar *)


#hanoipillar3
(name *)		right Hanoi pillar
(dict *)		third
(descr *)		A pillar on the right of the Towers of Hanoi set. On it is (a list of disks on *).
(supporter *)
(* is #partof #hanoi)
(hanoipillar *)


(appearance $Disk #on $Pillar)
	(hanoidisk $ $Disk)
	(hanoipillar $Pillar)



%% Hanoi disks


(hanoidisk #hanoidiskempty 10)
%% the dummy disk is just there to represent an empty pillar

(proper $D) (hanoidisk $ $D)
(name $D)	(hanoidisk $X $D) disk $X 
(item $D)	(hanoidisk $X $D)($X < 10)
($D is #on #hanoipillar1)	(hanoidisk $X $D)($X < 10)
(descr $Disk)
	(hanoidisk $ $Disk)
	It's (the $Disk) of the Hanoi set.

(hanoidisk 1 #hanoidisk1)
(hanoidisk 2 #hanoidisk2)
(hanoidisk 3 #hanoidisk3)
(hanoidisk 4 #hanoidisk4)
(hanoidisk 5 #hanoidisk5)




(instead of [put $Disk1 #on $Disk2])
	(hanoidisk $ $Disk1)
	(hanoidisk $ $Disk2)
	($Disk2 is #on $Pillar)
	(hanoipillar $Pillar)

	(try [put $Disk1 #on $Pillar])

(prevent [put $Thing #on $Pillar])
	(hanoipillar $Pillar)
	~(hanoidisk $ $Thing)
	
	You can only put a Towers of Hanoi disk on one of the Towers of Hanoi pillars.

~(prevent [put $Disk #on $Pillar])
	(hanoipillar $Pillar)
	(hanoidisk $ $Disk)
	~($ is #on $Pillar)
	

(prevent [put $Disk #on $Pillar])
	(hanoipillar $Pillar)
	(hanoidisk $Size $Disk)
	(top disk on $Pillar is $Topdisk)
	(hanoidisk $Topsize $Topdisk)
	($Size > $Topsize)
	
	You can't put (the $Disk) on top of (the $Topdisk) on (the $Pillar) because it's too large. Try a smaller disk, or a different pillar.


(prevent [take $Disk])
	(hanoidisk $ $Disk)
	($Disk is #on $Pillar)
	(top disk on $Pillar is $Topdisk)
	~($Topdisk = $Disk)

	You can't take (the $Disk) because it's not the top disk on (the $Pillar). You would need to take (the $Topdisk) first.

(unlikely [examine $Thing])
	(hanoidisk $ $Thing)
	(or)
	(hanoipillar $Thing)


%% same Hanoi rules for robot

%% can only take top disk

($ prevent [take $Disk] [not top $Pillar])
	(hanoidisk $ $Disk)
	($Disk is #on $Pillar)
	(hanoipillar $Pillar)
	(top disk on $Pillar is $Topdisk)
	~($Topdisk = $Disk)

($Actor narrate [take $Disk] [not top $Pillar])
	(The $Actor) tr (ies $Actor) to take (the $Disk) from (the $Pillar), but can't because it's not the topmost one.


%% can only put disk on a larger disk

($ prevent [put $Disk #on $Pillar] [too small $Topdisk])
	(hanoipillar $Pillar)
	(hanoidisk $Size $Disk)
	(top disk on $Pillar is $Topdisk)
	(hanoidisk $Topsize $Topdisk)
	($Size > $Topsize)

($Actor narrate [put $Disk #on $Pillar] [too small $Topdisk])
	(The $Actor) tr (ies $Actor) to put (the $Disk) on (the $Pillar), but can't because (the $Topdisk) is too small.

(disks $Disks on pillar $Pillar)
	(list of disks on $Pillar is $Pdisks)
	(append $Disks $ $Pdisks)

%% find a free pillar given one or two other pillars

(freepillar $H1 $H2 $H3)
	*(hanoipillar $H3)
	~($H3 = $H1)
	~($H3 = $H2)

%% goals and plans

%% Hanoi is reset when all disks are on the left pillar
%% Hanoi is solved when all disks are on the right pillar

(fact [#hanoi reset] only when [[#hanoidisk5 stackedon #hanoipillar1]])

(describe fact [#hanoi reset])
	Hanoi is reset.

(fact [#hanoi solved] only when [[#hanoidisk5 stackedon #hanoipillar3]])

(describe fact [#hanoi solved])
	Hanoi is solved.

	

%% a disk is stacked on a pillar if it and all smaller disks are on the pillar


(next smallest disk from $Disk is $Nextdisk)
	(hanoidisk $Disksize $Disk)
	($Disksize minus 1 into $Nextsize)
	(hanoidisk $Nextsize $Nextdisk)

(fact [$Disk stackedon $Pillar])
	(hanoipillar $Pillar)
	(hanoidisk $Disksize $Disk)
	($Disk is #on $Pillar)
	{
		($Disksize = 1)
		(or)
		(next smallest disk from $Disk is $Nextdisk)
		(fact [$Nextdisk stackedon $Pillar])
	}

(describe fact [$Disk stackedon $Pillar])
	(the $Disk) is fully stacked on (the $Pillar)

($A plan [$Disk stackedon $Pillar] $P)
	(hanoipillar $Pillar)
	(hanoidisk $Disksize $Disk)
	
	(if)	%% if we're holding the disk and there's space on the pillar, put it there
		
		($Disk is #heldby $A)
		(top disk on $Pillar is $Topdisk)
		(hanoidisk $Topsize $Topdisk)
		($Disksize < $Topsize)


	(then) ($P = [[$A canreach $Pillar][do put $Disk #on $Pillar]])

	(elseif) %% If it's the smallest disk, just take it	

		($Disksize = 1)

	(then)	($P = [[$Disk #heldby $A]])

	(elseif) %% if this disk is already on the pillar, just make sure the next disk is stacked here as well

		($Disk is #on $Pillar)
		(next smallest disk from $Disk is $Nextdisk)

	(then)	($P = [[$Nextdisk stackedon $Pillar]])
		
	(elseif) 	%% if this disk needs to be moved, stack the disk above onto the free pillar then take the now-exposed disk
			%% relying on stacking the disks to also clear space on the to pillar

		($Disk is #on $Frompillar)
		(hanoipillar $Frompillar)
	
		(next smallest disk from $Disk is $Nextdisk)
		(freepillar $Pillar $Frompillar $Freepillar)

	(then)	($P = [[$Nextdisk stackedon $Freepillar] [$A canreach $Disk] [do take $Disk]])

	(else)		%% the disk is where the heck?? Go find it!
			($P = [[$Disk #heldby $A]])
		
	(endif)		

	




