# Planner for Dialog

A goal-planning extension for the Interactive Fiction language Dialog.

This is a ground-up rewrite for the Dialog language ( https://www.linusakesson.net/dialog/ ) of my "Reactive Agent Planner" library which goes back to the late 1990s ( http://www.ifwiki.org/index.php/Reactive_Agent_Planner )

With a decent Prolog behind it, maybe finally Planner can start to make some sense. 

I cribbed the original concept of the Planner engine from the CMU Oz Project's 1991 ( http://www.cs.cmu.edu/afs/cs.cmu.edu/project/oz/web/papers.html ) Hap architecture, and the name from Carl Hewitt's language (https://en.wikipedia.org/wiki/Planner_(programming_language)  used to implement Terry Winograd's 1968 SHRDLU (https://en.wikipedia.org/wiki/SHRDLU).

It is my hope that, at some point in the 2020s, the hobbyist Interactive Fiction community, which has built itself on reengineered discarded MIT tools from 1979 (https://en.wikipedia.org/wiki/Z-machine), might one day be able to catch up with the state of the art of Artificial Intelligence in 1969. If we work extremely hard and are extremely lucky, in a distant future we might even be able to get all the way back to 1959 (https://en.wikipedia.org/wiki/General_Problem_Solver). But I don't dare to dream that far yet.

The Planner engine is much simplified from Hap as described in http://www.cs.cmu.edu/afs/cs.cmu.edu/project/oz/web/papers/CMU-CS-91-147.ps . Since nobody on the planet now has a computer that will read raw Postscript, I have uploaded a PDF conversion here, https://gitlab.com/natecull/planner-for-dialog/-/blob/master/CMU-CS-91-147.pdf , and I hope the authors don't object. 

Unlike Hap, Planner doesn't store a memory of previous plans, and it has only one plan type: a breadth-first search of goals that must be achieved in a strict sequence. It doesn't include any mechanisms for learning. It leaves everything else up to the story author to provide appropriate hints for tricky situations.

It is maybe interesting to compare the Oz Project with modern Interactive Fiction environments. It certainly was an ambitious project. Probably a little too ambitious - there is not much discussion about 'playability' or any indication really that they were creating a game design framework, or even thought in those terms. Hap was only a tiny part of this project.

I stumbled on the ruins of Oz around 1995. As far as I know, I may be the only person who has read any of the papers since 1991. The AI Winter, the World Wide Web and Wolfenstein 3D all happened then, and the world moved away from the Lisp-style AI track.

To get the most out of the testbed, you will need to have Dialog installed, and use dgdebug rather than dialogc. Try:

`dgdebug testbed.dg planner.dg stddebug.dg stdlib.dg`

and good luck.